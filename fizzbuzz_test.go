package fizzbuzz

import (
	"fmt"
	"reflect"
	"testing"
)

func TestConvert(t *testing.T) {
	testCase := []struct {
		input    int
		expected string
	}{
		{1, "1"},
		{2, "2"},
		{3, "Fizz"},
		{4, "4"},
		{5, "Buzz"},
		{6, "Fizz"},
		{15, "FizzBuzz"},
	}

	for _, tc := range testCase {
		t.Run(fmt.Sprintf("%v", tc.input), func(t *testing.T) {
			actual := convert(tc.input)
			if tc.expected != actual {
				t.Errorf("expected %+v but return %v", tc.expected, actual)
			}
		})
	}
}

func TestGenerate(t *testing.T) {
	testCase := []struct {
		input    int
		expected []string
	}{
		{
			15,
			[]string{"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"},
		},
	}

	for _, tc := range testCase {
		t.Run(fmt.Sprintf("%v", tc.input), func(t *testing.T) {
			actual := generate(tc.input)
			if !reflect.DeepEqual(tc.expected, actual) {
				t.Errorf("expected %+v \nbut return %v", tc.expected, actual)
			}
		})
	}

}
