package fizzbuzz

import "fmt"

func convert(num int) string {
	if num%15 == 0 {
		return "FizzBuzz"
	}
	if num%3 == 0 {
		return "Fizz"
	}
	if num%5 == 0 {
		return "Buzz"
	}
	return fmt.Sprintf("%d", num)
}

func generate(num int) []string {
	answer := []string{}
	for i := 1; i <= num; i++ {
		answer = append(answer, convert(i))
	}
	return answer
}
