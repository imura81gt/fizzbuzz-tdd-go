FuzzBuzz go
===========================

Specification
---------------------------

equal test case.

- [x] output String result
    - [x] If 1, return "1".
- [x] If multiples of 3, output `Fizz`
    - [x] If 3, return "Fizz".
- [x] If multiples of 5, output `Buzz`
    - [x] If 5, return "Buzz".
- [x] If multiples of 3 and multiples of 5, output `FizzBuzz`
    - [x] If 15, return "FizzBuzz".
- [x] If loop 1-100, return slice.
    - If loop 15 counts, return ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"].


Table Driven Test
------------------------------

> Writing good tests is not trivial, but in many situations a lot of ground can be covered with table-driven tests:

- [golang/go/wiki/TableDrivenTests](https://github.com/golang/go/wiki/TableDrivenTests)

